package orders;
import javax.swing.JFrame;
import java.sql.*;
public class SwingDemo {
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createGUI();
			}
		});
		
	}
	public static void createGUI() {
		myFrame frame=new myFrame("swingdemo!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600,400);
		frame.setVisible(true);
	}
}
