package orders;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.sql.*;
public class myFrame extends JFrame{
	JLabel label=new JLabel("廠商名");
	JLabel label2=new JLabel("統一編號");
	JLabel label3=new JLabel("請輸入欲刪除編號資料");
	//寬度顯示16列
	JTextField textField=new JTextField(16);
	JTextField textField2=new JTextField(16);
	JTextField textField3=new JTextField(16);
	JButton button =new JButton("刪除");
	JButton button3 =new JButton("查詢");
	JButton button2 =new JButton("確定");
	public myFrame(String title) {
		super(title);
		//內容
		Container contentPane=getContentPane();
		contentPane.setLayout(new FlowLayout());
		//控件
		contentPane.add(label);
		contentPane.add(textField);
		contentPane.add(label2);
		contentPane.add(textField2);
		contentPane.add(button2);
		contentPane.add(button3);
		contentPane.add(label3);
		contentPane.add(textField3);
		contentPane.add(button);
		button.addActionListener((e)->{onButtonOk2();});
		button2.addActionListener((e)->{onButtonOk();});
		button3.addActionListener((e)->{onButtonOk3();});
		
	}
	private void onButtonOk() {
		String str=textField.getText();
		String str2=textField2.getText();
		if(str.equals("")||str2.equals("")) {
			if(str.equals("")&&str2.equals("")) {
				Object[] options = {"OK","CANCEL"};
				JOptionPane.showOptionDialog(null, "你沒有輸入廠商名稱和統一編號","提示",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,options,options[0]);
			}
			else if(str.equals("")) {
				Object[] options = {"OK","CANCEL"};
				JOptionPane.showOptionDialog(null, "你沒有輸入廠商名稱","提示",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,options,options[0]);
			}
			else {
				Object[] options = {"OK","CANCEL"};
				JOptionPane.showOptionDialog(null, "你沒有輸入統一編號","提示",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,options,options[0]);
			}
			
	    }
		else {
			JOptionPane.showMessageDialog(this,"你輸入了:"+str+"+"+str2);
			String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; //載入JDBC驅動
			String dbURL = "jdbc:sqlserver://ROG-WIN10\\MSSQL2019;databaseName=orders"; //連線伺服器和資料庫test
			//String dbURL = "jdbc:sqlserver://localhost:1433;databaseName=orders"; //連線伺服器和資料庫test
			String userName = "test"; //預設使用者名稱
			String userPwd = "test"; //密碼
			
			Connection dbConn;
			try {
			Class.forName(driverName);
			dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
			System.out.println("Connection Successful!"); //如果連線成功 控制檯輸出Connection Successful!
			String sql="INSERT INTO orders (uniformnumber,vendor)"+"VALUES (?,?)";
			//String selecttest="select * from orders where uniformnumber=\"測試1\"";
			//Statement stmt =dbConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			PreparedStatement statement =dbConn.prepareStatement(sql);
			
			statement.setString(1,str2);
			statement.setString(2, str);
			int rows=statement.executeUpdate();
			if(rows>0) {
				System.out.println("Insert sucsess");
			}
			} catch (Exception e) {
			e.printStackTrace();
			}
		}
			
	}
	private void onButtonOk2() {
		//String str=textField.getText();
		//String str2=textField2.getText();
		String str=textField3.getText();
		if(str.equals("")) {
			Object[] options = {"確認","取消"};
			JOptionPane.showOptionDialog(null, "你沒有輸入要刪除的編號","提示",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,options,options[0]);
	    }
		else {
			JOptionPane.showMessageDialog(this,"你輸入了:"+str);
			String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; //載入JDBC驅動
			String dbURL = "jdbc:sqlserver://ROG-WIN10\\MSSQL2019;databaseName=orders"; //連線伺服器和資料庫test
			//String dbURL = "jdbc:sqlserver://localhost:1433;databaseName=orders"; //連線伺服器和資料庫test
			String userName = "test"; //預設使用者名稱
			String userPwd = "test"; //密碼
			
			Connection dbConn;
			try {
			Class.forName(driverName);
			dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
			System.out.println("Connection Successful!"); //如果連線成功 控制檯輸出Connection Successful!
			String sql="DELETE FROM orders WHERE NO =? ";
			//String selecttest="select * from orders where uniformnumber=\"測試1\"";
			//Statement stmt =dbConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			PreparedStatement statement =dbConn.prepareStatement(sql);
			
			statement.setString(1,str);
			int rows=statement.executeUpdate();
			if(rows>0) {
				JOptionPane.showMessageDialog(null,"Deleted Successfully!");
			}
			else {
				JOptionPane.showMessageDialog(null,"value does not exists!");
			}
			} catch (Exception e) {
			e.printStackTrace();
			}
		}
			
	}
	private void onButtonOk3() {
		String str=textField.getText();
		String str2=textField2.getText();
		if(str.equals("")) {
			Object[] options = {"OK","CANCEL"};
			JOptionPane.showOptionDialog(null, "你沒有輸入廠商名稱","提示",JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,options,options[0]);
	    }
		else {
			JOptionPane.showMessageDialog(this,"你輸入了:"+str+"+"+str2);
			String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; //載入JDBC驅動
			String dbURL = "jdbc:sqlserver://ROG-WIN10\\MSSQL2019;databaseName=orders"; //連線伺服器和資料庫test
			//String dbURL = "jdbc:sqlserver://localhost:1433;databaseName=orders"; //連線伺服器和資料庫test
			String userName = "test"; //預設使用者名稱
			String userPwd = "test"; //密碼
			
			Connection dbConn;
			try {
			Class.forName(driverName);
			dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
			System.out.println("Connection Successful!"); //如果連線成功 控制檯輸出Connection Successful!
			String sql="SELECT * FROM orders WHERE vendor like ?";
			//String selecttest="select * from orders where uniformnumber=\"測試1\"";
			//Statement stmt =dbConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			PreparedStatement statement =dbConn.prepareStatement(sql);
			
			statement.setString(1,"%"+str+"%");
			//statement.setString(2, str);
			ResultSet rs = statement.executeQuery();
			String[] rss;
			rss=new String[100];
			int rsscount=0;
			while(rs.next()) {
				rss[rsscount]="第"+rs.getRow()+"筆||編號為"+rs.getInt(1)+"||名稱為"+rs.getString(2)+"||統一編號為"+rs.getString(3)+"\t";
				System.out.println("第"+rs.getRow()+"筆");
				System.out.print(rs.getInt(1)+"\t");
				System.out.print(rs.getString(2)+"\t");
				System.out.println(rs.getString(3)+"\t");	
				rsscount++;
			}
			String getFavFruit = (String) JOptionPane.showInputDialog(
	                null,
	                "What fruit do you like the most?",
	                "Choose Fruit",
	                JOptionPane.QUESTION_MESSAGE,
	                null,
	                rss,
	                rss[0]);
			//int rows=statement.executeUpdate();
			//if(rows>0) {
			//	System.out.println("SELECT sucsess");
			//}
			} catch (Exception e) {
			e.printStackTrace();
			}
		}
			
	}
}
